﻿using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Primecomp93Helix.Feature.PageContent.Models;
namespace Primecomp93Helix.Feature.PageContent.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MiniLogin()
        {
            return PartialView();
        }
        
        public ActionResult Submit(string username, string password)
        {
            var auth = false;
            Login login = new Login()
            {
                Password = password,
                UserName = username
            };
            try
            {
                Sitecore.Diagnostics.Log.Info("Trying to login as: " + login.UserName, login);
                if (!username.Contains("\""))
                {
                    login.UserName = @"sitecore\" + username;
                    auth = Sitecore.Security.Authentication.AuthenticationManager.Login(login.UserName, login.Password, false);
                }

                
                if (!auth)
                {
                    login.UserName = @"extranet\" + username;
                    auth = Sitecore.Security.Authentication.AuthenticationManager.Login(login.UserName, login.Password, false);
                }
                if (auth)
                {
                    Sitecore.Diagnostics.Log.Info("User Authenticated: " + login.UserName, login);
                    Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(login.UserName, true);
                    var rolesList = user.Roles.ToList();
                    bool admin = rolesList.Any(x => x.Name == @"sitecore\SiteAdmin");
                    //foreach (var role in rolesList)
                    //{
                    //    var roleName = role.DisplayName;
                    //    var roleDomain = role.Domain;
                    //    if (roleName.Contains("Admin"))
                    //    {
                    //        admin = true;
                    //        break;
                    //    }
                    //}
                    if (admin)
                    {
                        Item ReportCenter = Sitecore.Context.Database.GetItem("F54F3B9E-F479-463A-8D6F-94D1330C6CBD");
                        var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
                        defaultUrlOptions.EncodeNames = true;
                        
                        // get url
                        var url = LinkManager.GetItemUrl(ReportCenter, defaultUrlOptions);
                        return Redirect(url);
                    }
                    else
                    {
                        return Redirect("~/Home/");
                    }
                }
                else
                {
                    Sitecore.Diagnostics.Log.Info("Unable to Authenticate User: " + login.UserName, login);
                    Response.Cookies.Add(new System.Web.HttpCookie("incorrectPasswordOrUsernameError"));
                    return Redirect("~/Home/"); 
                }
            }
            catch(Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Exception while login(" + login.UserName + ") -" + e.Message + e.InnerException.Message, e, e.Data);
                return Redirect("/Home/"); 
            }
            
        }
    }
}