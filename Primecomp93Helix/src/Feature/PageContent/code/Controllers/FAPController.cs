﻿using Newtonsoft.Json;
using Primecomp93Helix.Feature.PageContent.Helpers;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using BingMapsRESTToolkit;
using Primecomp93Helix.Feature.PageContent.Models;
using RestSharp;
using Sitecore.Configuration;
using Sitecore.Diagnostics;
using Address = Primecomp93Helix.Feature.PageContent.Models.Address;

namespace Primecomp93Helix.Feature.PageContent.Controllers
{
    public class FAPController : Controller
    {
        private static readonly string Bingapi = Settings.GetSetting("bingapi");
        public ActionResult FapResults()
        {
            refreshCustomPipelines();
            return View();
        }
        public void refreshCustomPipelines()
        {
            try
            {
                Log.Info("Calling Custom Pipelines", this);
                // refresh pipelines
                var args = new PipelineArgs();
                CorePipeline.Run("customPipelines", args);
            }
            catch (Exception e)
            {
                Log.Error("PrimeComp93Helix.Feature.PageContent.Controllers.FAPController.refreshCustomPipelines ", e.InnerException.Message);
            }
        }
        public ActionResult FapProfile()
        {
            Physician physician = new Physician();
            Item profile = Sitecore.Context.Item;

            try
            {
                physician.SetFirstName(profile.Fields["FirstName"].Value);
                physician.SetMiddleName(profile.Fields["MiddleName"].Value);
                physician.SetLastName(profile.Fields["LastName"].Value);
                string gender;
                if (!string.IsNullOrEmpty(profile.Fields["Gender"].Value))
                {
                    if (profile.Fields["Gender"].Value.Equals("M"))
                    {
                        gender = "Male";
                    }
                    else if (profile.Fields["Gender"].Value.Equals("F"))
                    {
                        gender = "Female";
                    }
                    else
                    {
                        gender = "Unknown";
                    }
                }
                else
                {
                    gender = "Unknown";
                }
                physician.Gender = gender;
                physician.Email = profile.Fields["Email"].Value;
                physician.Languages = !string.IsNullOrEmpty(profile.Fields["Languages"].Value) ?
                    profile.Fields["Languages"].Value.Split(',') : null;
                physician.GroupName = profile.Fields["GroupName"].Value;
                physician.County = (profile.Fields["County"] != null) ? profile.Fields["County"].Value : null;
                physician.Address = profile.Fields["Address"].Value;
                physician.City = profile.Fields["City"].Value;
                physician.State = profile.Fields["State"].Value;
                physician.Zip = profile.Fields["Zip"].Value;
                physician.SetPhone(profile.Fields["Phone"].Value);
                physician.Fax = profile.Fields["Fax"].Value;
                physician.SecondaryFacilityAddress = profile.Fields["SecondaryFacilityAddress"].Value;
                physician.SecondaryFacilityCity = profile.Fields["SecondaryFacilityCity"].Value;
                physician.SecondaryFacilityState = profile.Fields["SecondaryFacilityState"].Value;
                physician.SecondaryFacilityZip = profile.Fields["SecondaryFacilityZip"].Value;
                physician.SecondaryFacilityPhone = profile.Fields["SecondaryFacilityPhone"].Value;
                physician.SecondaryFacilityFax = profile.Fields["SecondaryFacilityFax"].Value;
                physician.AdditionalFacilityAddress = profile.Fields["AdditionalFacilityAddress"].Value;
                physician.AdditionalFacilityCity = profile.Fields["AdditionalFacilityCity"].Value;
                physician.AdditionalFacilityState = profile.Fields["AdditionalFacilityState"].Value;
                physician.AdditionalFacilityZip = profile.Fields["AdditionalFacilityZip"].Value;
                physician.AdditionalFacilityPhone = profile.Fields["AdditionalFacilityPhone"].Value;
                physician.AdditionalFacilityFax = profile.Fields["AdditionalFacilityFax"].Value;
                physician.ProviderType = (profile.Fields["ProviderType"] != null) ? profile.Fields["ProviderType"].Value : null;
                physician.Specialty = profile.Fields["Specialty"].Value;
                physician.Degree = profile.Fields["Degree"].Value;
                physician.SubSpecialty = profile.Fields["SubSpecialty"].Value;
                physician.SpecialtyNotes = profile.Fields["SpecialtyNotes"].Value;
                physician.site = (profile.Fields["Site"] != null) ? profile.Fields["Site"].Value : null;
                physician.ID = profile.Name;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Primecomp.Controllers.FAPController :: FapResults -> Exception : " + ex.ToString(), ex, ex.Data);
                //throw;
            }
            return View(physician);
        }

        public Physician GetPhysicianProfile()
        {
            Physician physician = new Physician();
            return physician;
        }

        [HttpPost]
        public JsonResult PhysicianQuery(SitecoreQuery obj)
        {
            try
            {
                List<Item> master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (master == null)
                {
                    refreshCustomPipelines();
                    master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                }
                List<Item> list = new List<Item>();
                SitecoreResponse response = Search(master, obj);
                // Map Results
                List<PhysicianSearch> Physicians = MapPhysician(response.Results);


                // Map Views to Response
                ViewResponse json = new ViewResponse();

                // etc
                json.TotalHits = response.TotalHits;
                json.OriginalQuery = response.OriginalQuery;
                if (!string.IsNullOrEmpty(obj.Distance) && !string.IsNullOrEmpty(obj.SearchZip))
                {
                    Physicians = Physicians.OrderBy(x => x.Distance).ToList();
                }
                else if (obj.Sort != null && obj.Sort.Equals("desc"))
                {
                    Physicians = Physicians.OrderByDescending(x => x.GetLastName()).ThenBy(x => x.GetFirstName()).ToList();
                }
                else
                {
                    Physicians = Physicians.OrderBy(x => x.GetLastName()).ThenBy(x => x.GetFirstName()).ToList();
                }
                // results
                json.Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", new PhysicianResultsView()
                {
                    Start = obj.Start,
                    Physicians = Physicians
                });

                // facets
                RenderFacetView(response, "specialties_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "languages_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "gender_s", json, "_FacetGender");
                RenderFacetView(response, "providertype_s", json, "_FacetDropdown");

                return new JsonResult()
                {
                    Data = json,
                    MaxJsonLength = Int32.MaxValue
                };

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("PrimeComp.Controllers.FAPController -> PhysicianQuery : " + ex.ToString(), this);
                return BuildJsonErrorResponse(ex);
            }
        }
        public static SitecoreResponse Search(List<Item> master, SitecoreQuery obj)
        {
            //List<Item> list = new List<Item>();
            SitecoreResponse response = new SitecoreResponse();
            List<Item> temp = new List<Item>();
            List<string> providerTypeList = new List<string>();
            //List<Item> filteredList = new List<Item>();
            if (!obj.query.Equals("*"))
            {
                foreach (var item in master)
                {
                    try
                    {
                        // we always want to display ALL Provider Types in the drop down
                        if (item.Fields["ProviderType"] != null)
                        {
                            string providerType = item.Fields["ProviderType"].ToString();
                            if (!string.IsNullOrEmpty(providerType))
                            {
                                if (!providerTypeList.Contains(providerType))
                                {
                                    providerTypeList.Add(providerType);
                                }
                            }
                        }
                        StringBuilder fullmatch = new StringBuilder(item.Fields["FirstName"].ToString());
                        fullmatch = fullmatch.Append(" ");
                        fullmatch = fullmatch.Append(item.Fields["LastName"].ToString());

                        if (fullmatch.ToString().Equals(obj.query.ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            temp.Add(item);
                        }
                        else if (item.Fields["LastName"].Value.ToString().Equals(obj.query, StringComparison.InvariantCultureIgnoreCase))
                        {
                            temp.Add(item);
                        }
                        else if (item.Fields["FirstName"].Value.ToString().Equals(obj.query, StringComparison.InvariantCultureIgnoreCase))
                        {
                            temp.Add(item);
                        }
                        else if (item.Fields["Specialty"].Value.ToString().Equals(obj.query, StringComparison.InvariantCultureIgnoreCase) ||
                             item.Fields["SubSpecialty"].Value.ToString().ToUpper().Split('|').Contains(obj.query.ToUpper()))
                        {
                            temp.Add(item);
                        }
                        else if(item.Fields["GroupName"].Value.ToString().Equals(obj.query,
                            StringComparison.InvariantCultureIgnoreCase) || item.Fields["GroupName"].Value.ToString().ToUpper().Contains(obj.query.ToUpper()))
                        {
                            temp.Add(item);
                        }
                        else
                        {
                            if (item.Fields["LastName"].Value.ToUpper().ToString().Contains(obj.query.ToUpper()) ||
                                item.Fields["FirstName"].Value.ToUpper().ToString().Contains(obj.query.ToUpper()))
                            {
                                temp.Add(item);
                            }
                        }
                        
                    }
                    catch (Exception e)
                    {
                        Sitecore.Diagnostics.Log.Error("Primecomp.Controllers.FAPController.Search ", e.Message);
                        continue;
                    }
                    
                    //else
                    //{
                    //    filteredList = temp;
                    //}
                }
                try
                {
                    if (obj.Filters.Count > 0)
                    {
                        temp = Filter(temp, obj);
                    }
                    response.TotalHits = temp.Count;
                    response.OriginalQuery = obj;
                    //List<Item> newlist = new List<Item>();
                    if (!string.IsNullOrEmpty(obj.Distance) && !string.IsNullOrEmpty(obj.SearchZip))
                    {
                        response = GetClosestPhysicians(temp, obj, response);
                    }
                    else
                    {
                        response.Results = GetPhysicians(temp, obj);
                    }

                    //response.TotalHits = temp.Count;
                }

                catch (Exception e)
                {
                    Sitecore.Diagnostics.Log.Error("Primecomp.Controllers.FAPController.Search ", e.Message);
                }
            }
            else
            {
                try
                {
                    foreach (var item in master)
                    {
                        // we always want to display ALL Provider Types in the drop down
                        if (item.Fields["ProviderType"] != null)
                        {
                            string providerType = item.Fields["ProviderType"].ToString();
                            if (!string.IsNullOrEmpty(providerType))
                            {
                                if (!providerTypeList.Contains(providerType))
                                {
                                    providerTypeList.Add(providerType);
                                }
                            }
                        }
                    }
                    if (obj.Filters.Count > 0)
                    {
                        //filter results before sorting and picking
                        master = Filter(master, obj);
                    }
                    response.OriginalQuery = obj;
                    response.Results = new List<Dictionary<string, object>>();
                    response.TotalHits = master.Count;
                    if (!string.IsNullOrEmpty(obj.Distance) && !string.IsNullOrEmpty(obj.SearchZip))
                    {
                        response = GetClosestPhysicians(master, obj, response);
                    }
                    else
                    {
                        response.Results = GetPhysicians(master, obj);
                    }
                }
                catch (Exception e)
                {
                    Sitecore.Diagnostics.Log.Error("Primecomp.Controllers.FAPController.Search ", e.Message);
                }
            }

            List<string> specList = new List<string>();
            List<string> langList = new List<string>();
            List<string> genderList = new List<string>();
            
            foreach (var item in master)
            {
                try
                {
                    string specialty = item.Fields["Specialty"].ToString();
                    if (!string.IsNullOrEmpty(specialty) && !specList.Contains(specialty))
                    {
                        specList.Add(specialty);
                    }
                    string otherspecialties = item.Fields["SubSpecialty"].ToString();
                    if (!string.IsNullOrEmpty(otherspecialties))
                    {
                        if (!specList.Contains(otherspecialties))
                        {
                            specList.Add(otherspecialties);
                        }
                    }
                    string languages = item.Fields["Languages"].ToString();
                    if (!string.IsNullOrEmpty(languages))
                    {
                        string[] langs = languages.Split(',');
                        foreach (var l in langs)
                        {
                            if (!langList.Contains(l))
                            {
                                langList.Add(l);
                            }
                        }
                    }
                    string gender = item.Fields["Gender"].ToString();
                    if (!string.IsNullOrEmpty(gender))
                    {
                        string fullGender = "";
                        if (gender.Equals("M"))
                        {
                            fullGender = "Male";
                        }
                        else if (gender.Equals("F"))
                        {
                            fullGender = "Female";
                        }
                        else
                        {
                            fullGender = "Unknown";
                        }
                        if (!genderList.Contains(fullGender))
                        {
                            genderList.Add(fullGender);
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Sitecore.Diagnostics.Log.Error("Primecomp.Controllers.FAPController.Search ", e.Message);
                    continue;
                }
            }
            specList.Sort();
            langList.Sort();
            genderList.Sort();
            
            if (providerTypeList != null)
            {
                providerTypeList.Sort();
            }
            response.Facets = new Dictionary<string, List<string>>();

            response.Facets.Add("specialties_pipe", specList);
            response.Facets.Add("gender_s", genderList);
            response.Facets.Add("languages_pipe", langList);
            if (providerTypeList != null)
            {
                response.Facets.Add("providertype_s", providerTypeList);
            }
            return response;
        }
        public static SitecoreResponse GetClosestPhysicians(List<Item> temp, SitecoreQuery obj, SitecoreResponse response)
        {
            List<PhysicianObj> distanceItems = new List<PhysicianObj>();
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            foreach (var item in temp)
            {
                try
                {
                    PhysicianObj physician = new PhysicianObj();
                    Double dist = -1;

                    if (!string.IsNullOrEmpty(item.Fields["lat"].Value))
                    {
                        ZipCode zip = new ZipCode();
                        zip.lat = double.Parse(item.Fields["lat"].Value);
                        zip.lon = double.Parse(item.Fields["long"].Value);
                        zip.zip = item.Fields["Zip"].Value;
                        dist = zipCodeDistance(zip, obj.SearchZip);
                    }
                    if (dist != -1 && dist <= double.Parse(obj.Distance))
                    {
                        physician.City = item.Fields["City"].Value;
                        physician.Address = item.Fields["Address"].Value;
                        physician.ContentID = item.Name;
                        physician.County = item.Fields["County"].Value;
                        physician.Degree = item.Fields["Degree"].Value;
                        physician.Distance = Math.Round(dist, 2).ToString();
                        physician.Fax = item.Fields["Fax"].Value;
                        physician.FirstName = item.Fields["FirstName"].Value;
                        string gender = "";
                        if (item.Fields["Gender"].Value.Equals("M"))
                        {
                            gender = "Male";
                        }
                        else if (item.Fields["Gender"].Value.Equals("F"))
                        {
                            gender = "Female";
                        }
                        else
                        {
                            gender = "Unknown";
                        }
                        physician.Gender = gender;
                        physician.GroupName = item.Fields["GroupName"].Value;
                        physician.Languages = item.Fields["Languages"].Value;
                        physician.LastName = item.Fields["LastName"].Value;
                        physician.MiddleName = item.Fields["MiddleName"].Value;
                        physician.Path = LinkManager.GetItemUrl(item, defaultUrlOptions);
                        //physician.Path = item.Paths.FullPath;
                        physician.Phone = item.Fields["Phone"].Value;
                        physician.ProviderType = item.Fields["ProviderType"].Value;
                        physician.Specialty = item.Fields["Specialty"].Value;
                        physician.SpecialtyNotes = item.Fields["SpecialtyNotes"].Value;
                        physician.State = item.Fields["State"].Value;
                        physician.SubSpecialty = item.Fields["SubSpecialty"].Value;
                        physician.Zip = item.Fields["Zip"].Value;
                        physician.AdditionalFacilityAddress = item.Fields["AdditionalFacilityAddress"].Value;
                        physician.AdditionalFacilityCity = item.Fields["AdditionalFacilityCity"].Value;
                        physician.AdditionalFacilityFax = item.Fields["AdditionalFacilityFax"].Value;
                        physician.AdditionalFacilityPhone = item.Fields["AdditionalFacilityPhone"].Value;
                        physician.AdditionalFacilityState = item.Fields["AdditionalFacilityState"].Value;
                        physician.AdditionalFacilityZip = item.Fields["AdditionalFacilityZip"].Value;
                        physician.SecondaryFacilityAddress = item.Fields["SecondaryFacilityAddress"].Value;
                        physician.SecondaryFacilityCity = item.Fields["SecondaryFacilityCity"].Value;
                        physician.SecondaryFacilityFax = item.Fields["SecondaryFacilityFax"].Value;
                        physician.SecondaryFacilityPhone = item.Fields["SecondaryFacilityPhone"].Value;
                        physician.SecondaryFacilityState = item.Fields["SecondaryFacilityState"].Value;
                        physician.SecondaryFacilityZip = item.Fields["SecondaryFacilityZip"].Value;
                        distanceItems.Add(physician);
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
            response.TotalHits = distanceItems.Count;
            var children = distanceItems.OrderBy(e => Convert.ToDouble(e.Distance))
                .Skip(obj.Start)
                .Take(10)
                .ToList();

            foreach (var child in children)
            {
                try
                {
                    Dictionary<string, object> pair = new Dictionary<string, object>();
                    pair.Add("Address", child.Address);
                    pair.Add("City", child.City);
                    pair.Add("ContentID", child.ContentID);
                    pair.Add("County", child.County);
                    pair.Add("Degree", child.Degree);
                    pair.Add("Distance", child.Distance);
                    pair.Add("Fax", child.Fax);
                    pair.Add("AdditionalFacilityAddress", child.AdditionalFacilityAddress);
                    pair.Add("AdditionalFacilityCity", child.AdditionalFacilityCity);
                    pair.Add("AdditionalFacilityFax", child.AdditionalFacilityFax);
                    pair.Add("AdditionalFacilityPhone", child.AdditionalFacilityPhone);
                    pair.Add("AdditionalFacilityState", child.AdditionalFacilityState);
                    pair.Add("AdditionalFacilityZip", child.AdditionalFacilityZip);
                    pair.Add("FirstName", child.FirstName);
                    string gender = "";
                    if (child.Gender.Equals("M"))
                    {
                        gender = "Male";
                    }
                    else if (child.Gender.Equals("F"))
                    {
                        gender = "Female";
                    }
                    else
                    {
                        gender = "Unknown";
                    }
                    pair.Add("Gender", gender);
                    pair.Add("GroupName", child.GroupName);
                    pair.Add("Languages", child.Languages);
                    pair.Add("LastName", child.LastName);
                    pair.Add("MiddleName", child.MiddleName);
                    pair.Add("Path", child.Path);
                    pair.Add("Phone", child.Phone);
                    pair.Add("ProviderType", child.ProviderType);
                    pair.Add("SecondaryFacilityAddress", child.SecondaryFacilityAddress);
                    pair.Add("SecondaryFacilityCity", child.SecondaryFacilityCity);
                    pair.Add("SecondaryFacilityFax", child.SecondaryFacilityFax);
                    pair.Add("SecondaryFacilityPhone", child.SecondaryFacilityPhone);
                    pair.Add("SecondaryFacilityState", child.SecondaryFacilityState);
                    pair.Add("SecondaryFacilityZip", child.SecondaryFacilityZip);
                    pair.Add("Specialty", child.Specialty);
                    pair.Add("SpecialtyNotes", child.SpecialtyNotes);
                    pair.Add("State", child.State);
                    pair.Add("SubSpecialty", child.SubSpecialty);
                    pair.Add("Zip", child.Zip);

                    list.Add(pair);
                }
                catch (Exception)
                {
                    continue;
                }
            }
            list.OrderBy(dict => dict["Distance"]);
            response.Results = list;
            return response;
        }
        public static List<Dictionary<string, object>> GetPhysicians(List<Item> master, SitecoreQuery obj)
        {
            List<Item> list = new List<Item>();
            List<Dictionary<string, object>> returnList = new List<Dictionary<string, object>>();
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            // response.OriginalQuery.Filters.TryGetValue(facetField, out filter)
            var filter = "";
            obj.Filters.TryGetValue("providertype_s", out filter);
            if (filter.Equals("Ancillary"))
            {
                if (obj.Sort != null && obj.Sort.Equals("desc"))
                {
                    list = master.OrderByDescending(x => x.Fields["GroupName"].Value.ToString())
                        .Skip(obj.Start)
                        .Take(10)
                        .ToList();
                }
                else
                {
                    list = master.OrderBy(x => x.Fields["GroupName"].Value.ToString())
                        .Skip(obj.Start)
                        .Take(10)
                        .ToList();
                }
            }
            else
            {
                if (obj.Sort != null && obj.Sort.Equals("desc"))
                {
                    list = master.OrderByDescending(x => x.Fields["LastName"].Value.ToString())
                        .ThenByDescending(x => x.Fields["FirstName"].Value.ToString())
                        .Skip(obj.Start)
                        .Take(10)
                        .ToList();
                }
                else
                {
                    list = master.OrderBy(x => x.Fields["LastName"].Value.ToString())
                        .ThenByDescending(x => x.Fields["FirstName"].Value.ToString())
                        .Skip(obj.Start)
                        .Take(10)
                        .ToList();
                }
            }
            foreach (var child in list)
            {
                try
                {
                    Dictionary<string, object> pair = new Dictionary<string, object>();
                    pair.Add("Address", child.Fields["Address"].Value);
                    pair.Add("City", child.Fields["City"].Value);
                    pair.Add("ContentID", child.Name);
                    pair.Add("County", child.Fields["County"].Value);
                    pair.Add("Degree", child.Fields["Degree"].Value);
                    pair.Add("Fax", child.Fields["Fax"].Value);
                    pair.Add("AdditionalFacilityAddress", child.Fields["AdditionalFacilityAddress"].Value);
                    pair.Add("AdditionalFacilityCity", child.Fields["AdditionalFacilityCity"].Value);
                    pair.Add("AdditionalFacilityFax", child.Fields["AdditionalFacilityFax"].Value);
                    pair.Add("AdditionalFacilityPhone", child.Fields["AdditionalFacilityPhone"].Value);
                    pair.Add("AdditionalFacilityState", child.Fields["AdditionalFacilityState"].Value);
                    pair.Add("AdditionalFacilityZip", child.Fields["AdditionalFacilityZip"].Value);
                    pair.Add("FirstName", child.Fields["FirstName"].Value);
                    string gender = "";
                    if (child.Fields["Gender"].Value.Equals("M"))
                    {
                        gender = "Male";
                    }
                    else if (child.Fields["Gender"].Value.Equals("F"))
                    {
                        gender = "Female";
                    }
                    else
                    {
                        gender = "Unknown";
                    }
                    pair.Add("Gender", gender);
                    pair.Add("GroupName", child.Fields["GroupName"].Value);
                    pair.Add("Languages", child.Fields["Languages"].Value);
                    pair.Add("LastName", child.Fields["LastName"].Value);
                    pair.Add("MiddleName", child.Fields["MiddleName"].Value);
                    pair.Add("Path", LinkManager.GetItemUrl( child, defaultUrlOptions));

                   // pair.Add("Path", child.Paths.FullPath);
                    pair.Add("Phone", child.Fields["Phone"].Value);
                    pair.Add("ProviderType", child.Fields["ProviderType"].Value);
                    pair.Add("SecondaryFacilityAddress", child.Fields["SecondaryFacilityAddress"].Value);
                    pair.Add("SecondaryFacilityCity", child.Fields["SecondaryFacilityCity"].Value);
                    pair.Add("SecondaryFacilityFax", child.Fields["SecondaryFacilityFax"].Value);
                    pair.Add("SecondaryFacilityPhone", child.Fields["SecondaryFacilityPhone"].Value);
                    pair.Add("SecondaryFacilityState", child.Fields["SecondaryFacilityState"].Value);
                    pair.Add("SecondaryFacilityZip", child.Fields["SecondaryFacilityZip"].Value);
                    pair.Add("Specialty", child.Fields["Specialty"].Value);
                    pair.Add("SpecialtyNotes", child.Fields["SpecialtyNotes"].Value);
                    pair.Add("State", child.Fields["State"].Value);
                    pair.Add("SubSpecialty", child.Fields["SubSpecialty"].Value);
                    pair.Add("Zip", child.Fields["Zip"].Value);

                    returnList.Add(pair);
                }
                catch (Exception )
                {
                    continue;
                }
            }
            return returnList;
        }

        private static ZipCode GetLatLong(Address address)
        {
          var zip = new ZipCode();
          try
          {
            if (address != null && !string.IsNullOrEmpty(address.Zip))
            {
              var client = new RestClient(Bingapi);
              var request = new RestRequest(Method.GET);
              request.AddHeader("Address", JsonConvert.SerializeObject(address));
              var response = client.Execute(request);
              if (!response.IsSuccessful) return null;

              try
              {
                var coords = JsonConvert.DeserializeObject<Coordinate>(response.Content);
                zip.lat = coords.Latitude;
                zip.lon = coords.Longitude;
              }
              catch (Exception e)
              {
                Log.Error("There was an error getting Lat/Long from Microservice ",e.Message);
              }
            }
          }
          catch (Exception ex)
          {
            Log.Error("Primecomp :: GetLatLong -> Could not obtain coordinates ",ex.Message);
          }
          return zip;
        }
        protected static double zipCodeDistance(ZipCode zip1, string zip2)
        {
            double distance;
            try
            {
                

                ZipCode zCode1 = zip1;
                Address searchAddress = new Address()
                {
                  StreetAddress = "",
                  City = "",
                  State =  "",
                  Country = "",
                  Zip = zip2
                };
                var zCode2 = GetLatLong(searchAddress); //ZipCode.selZipCode(zip2);

                if (zCode1 == null || zCode2 == null)
                {
                    throw new ArgumentException("Code does not exist");
                }

                double earthsRadius = 3956.08710710349;

                double latRadians = (zCode1.lat / 180) * Math.PI;
                double longRadians = (zCode1.lon / 180) * Math.PI;

                double latRadians2 = (zCode2.lat / 180) * Math.PI;
                double longRadians2 = (zCode2.lon / 180) * Math.PI;

                distance = (earthsRadius * 2) * Math.Asin(
                    Math.Sqrt(
                        Math.Pow(
                            Math.Sin((latRadians - latRadians2) / 2), 2) +
                                Math.Cos(latRadians) * Math.Cos(latRadians2) *
                                    Math.Pow(
                                        Math.Sin((longRadians - longRadians2) / 2), 2)));
            }
            catch(Exception e)
            {
                Log.Error("Exception :: zipCodeDistance " + e.Message, e.Message);
                distance = 0;
            }

            return distance;
        }
        private static List<Item> Filter(List<Item> list, SitecoreQuery obj)
        {
            List<Item> returnList = new List<Item>();
            string filter = null;
            int numFilters = 0;
            foreach (var filters in obj.Filters)
            {
                if (!string.IsNullOrEmpty(filters.Value))
                {
                    numFilters++;
                }
            }

            //filter specialties
            foreach (var items in list)
            {
                int numFiltersFound = 0;
                foreach (var filters in obj.Filters)
                {
                    if (filters.Key == "specialties_pipe")
                    {
                        filter = filters.Value;
                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (items.Fields["Specialty"].Value.ToString() == filter || items.Fields["SubSpecialty"].Value.ToString() == filter)
                            {
                                numFiltersFound++;
                            }
                        }
                    }
                    else if (filters.Key == "gender_s")
                    {
                        filter = filters.Value;
                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (!string.IsNullOrEmpty(items.Fields["Gender"].Value))
                            {
                                string gender = items.Fields["Gender"].Value;
                                if (gender == filter.Substring(0, 1))
                                {
                                    numFiltersFound++;
                                }
                            }
                        }
                    }
                    else if (filters.Key == "languages_pipe")
                    {
                        filter = filters.Value;
                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (items.Fields["Languages"].Value.ToString().Contains(filter))
                            {
                                numFiltersFound++;
                            }
                        }
                    }
                    else if (filters.Key == "providertype_s")
                    {
                        if (items.Fields["ProviderType"].Value != null)
                        {
                            filter = filters.Value;
                            if (!string.IsNullOrEmpty(filter))
                            {
                                if (items.Fields["ProviderType"].Value.ToString() == filter)
                                {
                                    numFiltersFound++;
                                }
                            }
                        }
                    }
                }
                if (numFiltersFound == numFilters && !returnList.Contains(items))
                {
                    returnList.Add(items);
                }
            }
            return returnList;
        }
        //[HttpPost]
        public JsonResult PhysicianTypeahead(SitecoreQuery obj)
        {
            try
            {
                // Execute Search
                List<Item> master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (master==null)
                {
                    refreshCustomPipelines();
                    master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                }
                SitecoreResponse response = Search(master, obj);
                // Map Results to Physician Model
                List<PhysicianSearch> Physicians = MapPhysician(response.Results);

                return Json(Physicians);
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }
        public static List<PhysicianSearch> MapPhysician(List<Dictionary<string, object>> results)
        {
            List<PhysicianSearch> Physicians = new List<PhysicianSearch>();
            PropertyInfo[] properties = typeof(PhysicianSearch).GetProperties();

            foreach (Dictionary<string, object> doc in results)
            {
                try
                {
                    PhysicianSearch physician = new PhysicianSearch();

                    foreach (PropertyInfo prop in properties)
                    {
                        try
                        {
                            object value = "";
                            if (doc.TryGetValue(prop.Name, out value))
                            {
                                if (prop.PropertyType == typeof(string[]))
                                {
                                    string[] strMultiData = value.ToString().Split(',');
                                    prop.SetValue(physician, strMultiData);
                                }
                                else if (prop.PropertyType == typeof(Single))
                                {
                                    prop.SetValue(physician, Convert.ToSingle(value));
                                }
                                else
                                    prop.SetValue(physician, value);
                            }
                            else
                            {
                                prop.SetValue(physician, null);
                            }
                        }
                        catch (Exception )
                        {
                            continue;
                        }
                    }
                    Physicians.Add(physician);
                }
                catch (Exception )
                {
                    continue;
                }
            }
            return Physicians;
        }
        //[HttpPost]
        //public JsonResult PhysicianFacets(SitecoreQuery obj)
        //{
        //    try
        //    {
        //        List<Item> master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
        //        if (master==null)
        //        {
        //            refreshCustomPipelines();
        //            master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
        //        }
        //        SitecoreResponse response = Search(master, obj);

        //        return Json(RenderFacetList(response, obj));
        //    }
        //    catch (Exception e)
        //    {
        //        return BuildJsonErrorResponse(e);
        //    }
        //}

        private void RenderFacetView(SitecoreResponse response, string facetField, ViewResponse json, string partial)
        {
            FacetView facetView = new FacetView();
            List<string> facets = new List<string>();
            try
            {
                if (response.Facets.TryGetValue(facetField, out facets))
                {
                    string filter = "";
                    if (response.OriginalQuery.Filters.TryGetValue(facetField, out filter))
                    {
                        facetView.Filter = filter.Split(',').ToList();
                    }
                    facetView.Facet = new KeyValuePair<string, List<string>>(facetField, facets);
                    json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Primecomp.Controllers.FAPController.RenderFacetView ", e.Message);
            }
        }

        //private Dictionary<string, List<string>> RenderFacetList(SitecoreResponse response, SitecoreQuery obj)
        //{
        //    Dictionary<string, List<string>> Facets = new Dictionary<string, List<string>>();
        //    foreach (string facetField in obj.Facets)
        //    {
        //        try
        //        {
        //            List<string> facets = new List<string>();
        //            if (response.Facets.TryGetValue(facetField, out facets))
        //            {
        //                Facets.Add(facetField, facets);
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Sitecore.Diagnostics.Log.Error("Primecomp.Controllers.FAPController.RenderFacetList ", e.Message);
        //        }
        //    }

        //    return Facets;
        //}

        private JsonResult BuildJsonErrorResponse(Exception e)
        {
            Dictionary<string, object> error = new Dictionary<string, object>();
            error.Add("Status", "Error");
            // Comment Below to Hide Debugging
            error.Add("Exception", new
            {
                Message = e.Message,
                Trace = e.StackTrace,
                InnerException = new
                {
                    Message = e.InnerException.Message != null ? e.InnerException.Message : "",
                    Trace = e.InnerException.StackTrace != null ? e.InnerException.StackTrace : ""
                }
            });

            return Json(error);
        }
    }
}
