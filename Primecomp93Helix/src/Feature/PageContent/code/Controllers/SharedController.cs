﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Primecomp93Helix.Feature.PageContent.Models;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Primecomp93Helix.Feature.PageContent.Controllers
{
    public class SharedController : Controller
    {
        private readonly string _headerItem = Settings.GetSetting("HeaderItem");

        private readonly string _headerDetails = Settings.GetSetting("HeaderDetails");

        private readonly string _blogId = Settings.GetSetting("BlogID");
        // GET: Shared
    
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Header()
        {
            Header _header = new Header
            {
                Menu = new Menu(),
                MenuItems = new List<MenuItem>(),
                Nav = new List<Nav>()
            };

            Sitecore.Diagnostics.Log.Debug("Header -> made it into _header: ");

            try
            {
                var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
                defaultUrlOptions.EncodeNames = true;
                ID menuItemId = new ID(_headerItem);
                Item HeaderMenuItem = Sitecore.Context.Database.GetItem(menuItemId);  // ("/sitecore/content/Components/Header Control/Header");

                ImageField imageField = HeaderMenuItem.Fields["Logo"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    MediaItem siteLogo = new MediaItem(imageField.MediaItem);
                    _header.Menu.SiteLogoURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(siteLogo));
                    _header.Menu.SiteLogo = siteLogo.Alt;
                }
                MultilistField headerMenuList = null;
                ID navList = null;
                string strHeaderDetails = _headerDetails;
                headerMenuList = HeaderMenuItem.Fields[strHeaderDetails];
                navList = new ID(strHeaderDetails);
                
                #region // Nav Links
                Item headerNavList = Sitecore.Context.Database.GetItem(navList);
                MultilistField headerNavMulList = null;
                headerNavMulList = headerNavList.Fields["Navigation Links"];
                ID strBlogID = new ID(_blogId);
                foreach (Item parentNavItem in headerNavMulList.GetItems())
                {
                    // don't build children for blog
                    
                    List<Item> childItems = null;
                    childItems = parentNavItem.GetChildren().ToList();
                    List<NavLinks> _childNavItem = new List<NavLinks>();
                    Sitecore.Diagnostics.Log.Info("BlogID= " + strBlogID.ToString() + " ParentNavItem.ID= " + parentNavItem.ID.ToString(), strBlogID);
                    if (strBlogID != parentNavItem.ID)
                    {
                        foreach (Item child in childItems)
                        {
                          // print out all the childitems
                          Sitecore.Diagnostics.Log.Info("Child Item: " + child.ID + " " + child.Name + " " + child.ParentID + " " + child.Parent.Name + " ", child);
                          if (child.ParentID == strBlogID)
                          {
                            Sitecore.Diagnostics.Log.Info("child.ParentID= " + child.ParentID + " == " + strBlogID, child);
                            continue;
                          }
                          _childNavItem.Add(new NavLinks
                          {
                              NavLinkText = child.DisplayName,

                              NavLinkURL = LinkManager.GetItemUrl(child, defaultUrlOptions) //.Paths.Path
                          });
                        }
                    }
                    _header.Nav.Add(new Nav
                    {
                        NavItemText = parentNavItem.DisplayName,
                        NavItemURL = LinkManager.GetItemUrl(parentNavItem, defaultUrlOptions), //parentNavItem.Paths.Path,
                        NavLinks = _childNavItem
                    });
                }
                #endregion
                return PartialView(_header);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info("Shared Header Exception : " + ex.ToString(), this);
                return null;
            }
            //return PartialView(_header);
        }
        public ActionResult HeaderFAP()
        {
            Header _header = new Header
            {
                Menu = new Menu(),
                MenuItems = new List<MenuItem>(),
                Nav = new List<Nav>()
            };

            Sitecore.Diagnostics.Log.Debug("Header -> made it into _header: ");

            try
            {
                var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
                defaultUrlOptions.EncodeNames = true;
                ID menuItemId = new ID(_headerItem);
                Item HeaderMenuItem = Sitecore.Context.Database.GetItem(menuItemId);  // ("/sitecore/content/Components/Header Control/Header");

                ImageField imageField = HeaderMenuItem.Fields["Logo"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    MediaItem siteLogo = new MediaItem(imageField.MediaItem);
                    _header.Menu.SiteLogoURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(siteLogo));
                    _header.Menu.SiteLogo = siteLogo.Alt;
                }
                MultilistField headerMenuList = null;
                ID navList = null;
                string strHeaderDetails = _headerDetails;
                headerMenuList = HeaderMenuItem.Fields[strHeaderDetails];
                navList = new ID(strHeaderDetails);

                #region // Nav Links
                Item headerNavList = Sitecore.Context.Database.GetItem(navList);
                MultilistField headerNavMulList = null;
                headerNavMulList = headerNavList.Fields["Navigation Links"];
                ID strBlogID = new ID(_blogId);
                foreach (Item parentNavItem in headerNavMulList.GetItems())
                {
                    // don't build children for blog

                    List<Item> childItems = null;
                    childItems = parentNavItem.GetChildren().ToList();
                    List<NavLinks> _childNavItem = new List<NavLinks>();
                    if (strBlogID != parentNavItem.ID)
                    {
                        foreach (Item child in childItems)
                        {
                            _childNavItem.Add(new NavLinks
                            {
                                NavLinkText = child.DisplayName,
                                NavLinkURL = LinkManager.GetItemUrl(child, defaultUrlOptions) //.Paths.Path
                                //NavLinkURL = child.Paths.Path
                            });
                        }
                    }
                    _header.Nav.Add(new Nav
                    {
                        NavItemText = parentNavItem.DisplayName,
                        NavItemURL = LinkManager.GetItemUrl(parentNavItem, defaultUrlOptions),
                        //NavItemURL = parentNavItem.Paths.Path,
                        NavLinks = _childNavItem
                    });
                }
                #endregion
                return PartialView(_header);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info("Shared Header Exception : " + ex.ToString(), this);
                return null;
            }
        }
        public ActionResult Footer()
        {
            return PartialView();
        }
        public ActionResult Breadcrumb()
        {
            Breadcrumb breadcrumb = new Breadcrumb();
            if (breadcrumb!=null)
            {
                return PartialView(breadcrumb);
            }
            else
            {
                return null;
            }
        }

        public ActionResult logout()
        {
            Sitecore.Security.Authentication.AuthenticationManager.Logout();
            Response.Cookies.Clear();
            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();
            return Redirect("/Home");
        }
    }
}