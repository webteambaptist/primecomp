﻿using Sitecore.Data.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Primecomp93Helix.Feature.PageContent.Models;

namespace Primecomp93Helix.Feature.PageContent.Controllers
{
    public class CarouselController : Controller
    {
        // GET: Carousel
        public ActionResult Index()
        {
            var items = Sitecore.Context.Database.SelectItems("/sitecore/content/Components/Carousel/*");
            var carouselFields = (from i in items
                                  select new CarouselComponent
                                  {
                                      ImageTitle = i.Fields["ImageTitle"].Value,
                                      ImageText = i.Fields["ImageText"].Value,
                                      ImgField = ((ImageField)i.Fields["BackgroundImage"])
                                  }).ToList();
            var carouselItems = new CarouselComponentItems
            { 
                _Items = carouselFields
            };
            
            return View(carouselItems);
        }
    }
}