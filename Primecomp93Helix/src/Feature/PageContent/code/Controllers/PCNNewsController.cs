﻿using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Xml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using Primecomp93Helix.Feature.PageContent.Models;
namespace Primecomp93Helix.Feature.PageContent.Controllers
{
    public class PCNNewsController : Controller
    {
        private static readonly string PcnNews = Settings.GetSetting("PCNNews");
        
        // GET: PCNNews
        public ActionResult Index()
        {
            Posts Posts = new Posts();
            Posts = GetPosts();
            
            return View(Posts);
        }
        public static string GetCategoryName(string categoryID)
        {
            Database db = Sitecore.Configuration.Factory.GetDatabase("web");
            categoryID = categoryID.Replace("{", "").Replace("}", "");
            Item Category = db.GetItem(categoryID);
            var category = Category.Fields["Category Name"].Value.ToString();
            return category;
        }
        public ActionResult Post()
        {
            var id = Request.QueryString["postID"];
            id = id.Replace("{", "");
            id = id.Replace("}","");
            Database db = Sitecore.Configuration.Factory.GetDatabase("web");
            Item parentItem = db.GetItem(id);

            BlogPost bp = new BlogPost();
            bp.title = parentItem.Fields["Title"].ToString();
            bp.Body = parentItem.Fields["Body"].ToString();
            var categoryID = parentItem.Fields["Category"].Value.ToString();
            bp.Category = GetCategoryName(categoryID);
            var publishDate = (DateField)parentItem.Fields["Publish Date"];
            var date = publishDate.DateTime;
            var dateString = date.ToString("MMMM d, yyyy");
            bp.PublishDate = dateString;
            bp.Summary = parentItem.Fields["Summary"].ToString();

            return View(bp);
        }
        public static string GetBlogPagination(int currentPage, Posts _posts)
        {
            int pages = 0;
            try
            {
                int industryPosts = _posts._industry.Count;
                int communityPosts = _posts._community.Count;

                if (industryPosts > communityPosts)
                {
                    pages = industryPosts;
                }
                else
                {
                    pages = communityPosts;
                }
            }
            catch (Exception )
            {
                pages = 0;
            }
            StringBuilder stringBuilder = new StringBuilder();
            if (pages > 1)
            {
                if (currentPage > pages)
                {
                    currentPage = 1;
                }
                if (currentPage != 1)
                {
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", "1", "First"));
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", Convert.ToString(currentPage - 1), "Prev"));
                }
                if (currentPage - 2 > 0)
                {
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", Convert.ToString(currentPage - 2), Convert.ToString(currentPage - 2)));
                }
                if (currentPage - 1 > 0)
                {
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", Convert.ToString(currentPage - 1), Convert.ToString(currentPage - 1)));
                }
                stringBuilder.Append(string.Format("{0} ", Convert.ToString(currentPage)));
                if (currentPage + 1 <= pages)
                {
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", Convert.ToString(currentPage + 1), Convert.ToString(currentPage + 1)));
                }
                if (currentPage + 2 <= pages)
                {
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", Convert.ToString(currentPage + 2), Convert.ToString(currentPage + 2)));
                }
                if (currentPage + 1 <= pages)
                {
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", Convert.ToString(currentPage + 1), "Next"));
                    stringBuilder.Append(string.Format("<a href=\"?{0}={1}\">{2}</a> ", "PCNNews", Convert.ToString(pages), "Last"));
                }
            }
            return stringBuilder.ToString();
        }
        public static Posts GetPosts()
        {
            Posts Posts = new Posts();
            Posts._industry = new List<Item>();
            Posts._community = new List<Item>();
            try
            { 
                // Map Results to Document Model
                Database db = Sitecore.Configuration.Factory.GetDatabase("web");
                Item parentItem = db.GetItem(PcnNews);

                var posts = parentItem.GetChildren().OrderByDescending(x => x.Created).ToList();
                foreach (var post in posts)
                {
                    string categoryID = null;
                    try
                    {
                        categoryID = post.Fields["Category"].Value.ToString();
                    }
                    catch(Exception)
                    {
                        continue;
                    }
                    if (!string.IsNullOrEmpty(categoryID))
                    {
                        var category = GetCategoryName(categoryID);

                        if (category.Equals("Prime Comp in the Community"))
                        {
                            Posts._community.Add(post);
                        }
                        else
                        {
                            Posts._industry.Add(post);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            return Posts;
        }
        public ActionResult CategoryPosts(string category)
        {
            Posts Posts = new Posts();
            var _posts = GetPosts();            

            if (category == "Prime Comp in the Community")
            {
                Posts._community = _posts._community;
                return View("Community", Posts);
            }
            else
            {
                Posts._industry = _posts._industry;
                return View("Industry", Posts);
            }
        }
    }
}