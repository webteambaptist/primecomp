﻿using Primecomp93Helix.Feature.PageContent.Helpers;
using Primecomp93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Pipelines;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Sitecore.Configuration;


namespace Primecomp93Helix.Feature.PageContent.Controllers
{
    public class ReportCenterController : Controller
    {
        private static readonly string StrReportCenterParentId = Settings.GetSetting("reportCenterParentID");
        // GET: ReportCenter
        public ActionResult Index()
        {
            if (Sitecore.Context.User.IsAuthenticated)
            {
                return View();

            }
            else
            {
                return Redirect("~/Home/");
            }
        }
        #region DocumentQuery
        [HttpPost]
        public JsonResult DocumentQuery(SitecoreQuery obj)
        {
            try
            {
                // Map Results to Document Model
                List<Document> Documents = new List<Document>();
                PropertyInfo[] properties = typeof(Document).GetProperties();

                Database db = Sitecore.Configuration.Factory.GetDatabase("web");
                Item parentItem = db.GetItem(StrReportCenterParentId);

                var children = parentItem.GetChildren(Sitecore.Collections.ChildListOptions.SkipSorting)
                    .OrderBy(f => f.Fields["Title"].ToString())
                    .ToList();
                
                SitecoreResponse response = Search(children, obj);

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    Document document = new Document();

                    foreach (PropertyInfo prop in properties)
                    {
                        object value = "";
                        if (doc.TryGetValue(prop.Name, out value))
                        {
                            // change thumbnail size, can handle from front end as well
                            if (prop.Name.Equals("Thumbnail"))
                            {
                                prop.SetValue(document, value.ToString().Replace("16", "50"));
                            }
                            else if (prop.Name.ToLower().Equals("extension"))
                            {
                                switch (value.ToString().ToLower())
                                {
                                    case "pdf":
                                        {
                                            prop.SetValue(document, "pdf.png");
                                            break;
                                        }
                                    case "doc":
                                    case "docx":
                                        {
                                            prop.SetValue(document, "word.png");
                                            break;
                                        }
                                    case "xls":
                                    case "xlsx":
                                        {
                                            prop.SetValue(document, "excel.png");
                                            break;
                                        }
                                    case "rtf":
                                        {
                                            prop.SetValue(document, "rtf-icon.png");
                                            break;
                                        }
                                    case "txt":
                                        {
                                            prop.SetValue(document, "txt-icon.png");
                                            break;
                                        }
                                    case "pptx":
                                        {
                                            prop.SetValue(document, "pptx-icon.png");
                                            break;
                                        }
                                    default:
                                        {
                                            prop.SetValue(document, "alternatedoc.png");
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                prop.SetValue(document, value);
                            }
                        }
                        else
                        {
                            prop.SetValue(document, null);
                        }
                    }
                    Documents.Add(document);
                }

                // Map Views to Response
                JsonResponse json = new JsonResponse();

                // etc
                json.TotalHits = response.TotalHits;
                json.OriginalQuery = response.OriginalQuery;

                //results
                json.Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", new DocumentResultView()
                {
                    Start = obj.Start,
                    Documents = Documents
                });

                // facets
                RenderFacetView(response, "__semantics", json, "_FacetCheckboxes");

                return Json(json);
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }
        #endregion

        #region DocumentSearch
        public static SitecoreResponse Search(List<Item> master, SitecoreQuery obj)
        {
            List<Item> list = new List<Item>();
            SitecoreResponse response = new SitecoreResponse();

            List<Item> temp = new List<Item>();
            List<Item> filteredList = new List<Item>();
            if (!obj.query.Equals("*"))
            {
                foreach (var item in master)
                {
                    StringBuilder fullmatch = new StringBuilder(item.Fields["Title"].ToString());

                    if (fullmatch.ToString().Equals(obj.query.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        temp.Add(item);
                    }
                    else
                    {
                        if (item.Fields["Title"].Value.ToUpper().ToString().Contains(obj.query.ToUpper()))
                        {
                            temp.Add(item);
                        }
                    }
                    if (obj.Filters.Count > 0)
                    {
                        filteredList = Filter(temp, obj);
                    }
                    else
                    {
                        filteredList = temp;
                    }
                }

                try
                {
                    List<Item> newlist = new List<Item>();
                    if (obj.Sort != null && obj.Sort.Equals("desc"))
                    {
                        list = filteredList.Where(x => x.Fields["Extension"].ToString() != "mdb")
                            .OrderByDescending(x => x.Fields["Title"].Value.ToString())
                            .ToList();
                        var filter = list.Skip(obj.Start)
                            .Take(10).ToList();
                        response.TotalHits = list.Count;
                    }
                    else
                    {
                        list = filteredList.Where(x=>x.Fields["Extension"].ToString()!="mdb")
                            .OrderBy(x => x.Fields["Title"].Value.ToString())
                            .ToList();
                        var filter = list.Skip(obj.Start)
                            .Take(10).ToList();
                        response.TotalHits = list.Count;
                    }

                    
                }

                catch (Exception e)
                {
                    Sitecore.Diagnostics.Log.Error("PhysicianPortal.Controllers.FAPController.Search ", e.Message);
                }
            }
            else
            {
                try
                {
                    if (obj.Filters.Count > 0)
                    {
                        //filter results before sorting and picking
                        filteredList = Filter(master, obj);
                    }
                    else
                    {
                        filteredList = master;
                    }
                    //list.Clear();
                    if (obj.Sort != null && obj.Sort.Equals("desc"))
                    {
                        var children = filteredList
                            .Where(x => x.Fields["Extension"].ToString() != "mdb")
                            .OrderByDescending(e => e.Fields["Title"].Value.ToString())
                            .ToList();
                        var filter = children.Skip(obj.Start)
                            .Take(10).ToList();
                        list = filter;
                        response.TotalHits = children.Count;
                    }
                    else
                    {
                        var children = filteredList
                            .Where(x => x.Fields["Extension"].ToString() != "mdb")
                            .OrderBy(e => e.Fields["Title"].Value.ToString())
                            .ToList();
                        var filter = children.Skip(obj.Start)
                            .Take(10).ToList();
                        list = filter;
                        response.TotalHits = children.Count;
                    }
                   
                }
                catch (Exception e)
                {
                    Sitecore.Diagnostics.Log.Error("PhysicianPortal.Controllers.FAPController.Search ", e.Message);
                }
            }
            response.OriginalQuery = obj;
            response.Results = new List<Dictionary<string, object>>();

            foreach (var doc in list)
            {
                //Update the model based on information from sitecore
                Dictionary<string, object> pair = new Dictionary<string, object>();
                pair.Add("Title", doc.Fields["Title"].Value);
                pair.Add("Path", doc.Paths.MediaPath);
                pair.Add("Created", doc.Statistics.Created);
                pair.Add("Updated", doc.Statistics.Updated);
                pair.Add("Extension", doc.Fields["Extension"].Value);
                response.Results.Add(pair);
            }

            //foreach (var r in list)
            //{
            //    Dictionary<string, object> pair = new Dictionary<string, object>();
            //    pair.Add("FirstName", r.Fields["FirstName"].Value);
            //    pair.Add("LastName", r.Fields["LastName"].Value);
            //    pair.Add("MiddleName", r.Fields["MiddleName"].Value);

            //    string gender = "";
            //    if (r.Fields["Gender"].Value.Equals("M"))
            //    {
            //        gender = "Male";
            //    }
            //    else if (r.Fields["Gender"].Value.Equals("F"))
            //    {
            //        gender = "Female";
            //    }
            //    else
            //    {
            //        gender = "Unknown";
            //    }
            //    pair.Add("Gender", gender);
            //    pair.Add("Specialty", r.Fields["Specialty"].Value);
            //    pair.Add("SubSpecialty", r.Fields["SubSpecialty"].Value);
            //    pair.Add("SpecialtyNotes", r.Fields["SpecialtyNotes"].Value);
            //    pair.Add("PhysicalAddress", r.Fields["PhysicalAddress"].Value);

            //    LocationInformation Location = new LocationInformation();
            //    Location.Fax = r.Fields["Fax"].Value;
            //    Location.City = r.Fields["City"].Value;
            //    Location.County = r.Fields["County"].Value;
            //    Location.Phone = r.Fields["Phone"].Value;
            //    Location.PhysicalAddress = r.Fields["PhysicalAddress"].Value;
            //    Location.State = r.Fields["State"].Value;
            //    Location.Zip = r.Fields["Zip"].Value;

            //    pair.Add("Location", Location);

            //    pair.Add("GroupName", r.Fields["GroupName"].Value);
            //    pair.Add("ProviderType", r.Fields["ProviderType"].Value);
            //    pair.Add("Degree", r.Fields["Degree"].Value);
            //    pair.Add("Languages", r.Fields["Languages"].Value);
            //    pair.Add("Path", r.Paths.FullPath);
            //    pair.Add("ContentID", r.Name);
            //    response.Results.Add(pair);
            //}

            //List<string> specList = new List<string>();
            //List<string> langList = new List<string>();
            //List<string> genderList = new List<string>();
            //List<string> providerTypeList = new List<string>();
            //foreach (var item in master)
            //{
            //    string specialty = item.Fields["Specialty"].ToString();
            //    if (!string.IsNullOrEmpty(specialty) && !specList.Contains(specialty))
            //    {
            //        specList.Add(specialty);
            //    }
            //    string otherspecialties = item.Fields["SubSpecialty"].ToString();
            //    if (!string.IsNullOrEmpty(otherspecialties))
            //    {
            //        if (!specList.Contains(otherspecialties))
            //        {
            //            specList.Add(otherspecialties);
            //        }
            //    }
            //    string languages = item.Fields["Languages"].ToString();
            //    if (!string.IsNullOrEmpty(languages))
            //    {
            //        string[] langs = languages.Split(',');
            //        foreach (var l in langs)
            //        {
            //            if (!langList.Contains(l))
            //            {
            //                langList.Add(l);
            //            }
            //        }
            //    }
            //    string gender = item.Fields["Gender"].ToString();
            //    if (!string.IsNullOrEmpty(gender))
            //    {
            //        string fullGender = "";
            //        if (gender.Equals("M"))
            //        {
            //            fullGender = "Male";
            //        }
            //        else if (gender.Equals("F"))
            //        {
            //            fullGender = "Female";
            //        }
            //        else
            //        {
            //            fullGender = "Unknown";
            //        }
            //        if (!genderList.Contains(fullGender))
            //        {
            //            genderList.Add(fullGender);
            //        }
            //    }
            //    string providerType = item.Fields["ProviderType"].ToString();
            //    if (!string.IsNullOrEmpty(providerType))
            //    {
            //        if (!providerTypeList.Contains(providerType))
            //        {
            //            providerTypeList.Add(providerType);
            //        }
            //    }
            //}
            //specList.Sort();
            //langList.Sort();
            //genderList.Sort();
            //providerTypeList.Sort();
            response.Facets = new Dictionary<string, List<string>>();

            //response.Facets.Add("specialties_pipe", specList);
            //response.Facets.Add("gender_s", genderList);
            //response.Facets.Add("languages_pipe", langList);
            //response.Facets.Add("providertype_s", providerTypeList);
            return response;
        }
        #endregion

        #region Filter
        public static List<Item> Filter(List<Item> list, SitecoreQuery obj)
        {
            List<Item> returnList = new List<Item>();
            string filter = null;
            int numFilters = 0;
            foreach (var filters in obj.Filters)
            {
                if (!string.IsNullOrEmpty(filters.Value))
                {
                    numFilters++;
                }
            }

            //filter specialties
            foreach (var items in list)
            {
                int numFiltersFound = 0;
                foreach (var filters in obj.Filters)
                {
                    if (filters.Key == "specialties_pipe")
                    {
                        filter = filters.Value;
                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (items.Fields["Specialty"].Value.ToString() == filter || items.Fields["SubSpecialty"].Value.ToString() == filter)
                            {
                                numFiltersFound++;
                            }
                        }
                    }
                    else if (filters.Key == "gender_s")
                    {
                        filter = filters.Value;
                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (!string.IsNullOrEmpty(items.Fields["Gender"].Value))
                            {
                                string gender = items.Fields["Gender"].Value;
                                if (gender == filter.Substring(0, 1))
                                {
                                    numFiltersFound++;
                                }
                            }
                        }
                    }
                    else if (filters.Key == "languages_pipe")
                    {
                        filter = filters.Value;
                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (items.Fields["Languages"].Value.ToString().Contains(filter))
                            {
                                numFiltersFound++;
                            }
                        }
                    }
                    else if (filters.Key == "providertype_s")
                    {
                        filter = filters.Value;
                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (items.Fields["ProviderType"].Value.ToString() == filter)
                            {
                                numFiltersFound++;
                            }
                        }
                    }
                }
                if (numFiltersFound == numFilters && !returnList.Contains(items))
                {
                    returnList.Add(items);
                }
            }
            return returnList;
        }
        #endregion
        public void refreshCustomPipelines()
        {
            try
            {
                // refresh pipelines
                var args = new PipelineArgs();
                CorePipeline.Run("customPipelines", args);
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("PrimeComp93Helix.Feature.PageContent.Controllers.FAPController.FAPResults ", e.InnerException.Message);
            }
        }
        #region DocumentTypeahead
        [HttpPost]
        public JsonResult DocumentTypeahead(SitecoreQuery obj)
        {
            try
            {
                // Execute Search
                List<Item> master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (master==null)
                {
                    refreshCustomPipelines();
                    master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                }
                SitecoreResponse response = Search(master, obj);

                // Map Results to Physician Model
                List<Document> Documents = new List<Document>();
                PropertyInfo[] properties = typeof(Document).GetProperties();

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    Document document = new Document();

                    foreach (PropertyInfo prop in properties)
                    {
                        object value = "";
                        if (doc.TryGetValue(prop.Name, out value))
                        {
                            prop.SetValue(document, value);
                        }
                        else
                        {
                            prop.SetValue(document, null);
                        }
                    }
                    Documents.Add(document);
                }

                return Json(Documents);
            }

            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }
        #endregion

        #region DocumentFacets
        [HttpPost]
        public JsonResult DocumentFacets(SitecoreQuery obj)
        {
            try
            {
                List<Item> master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (master==null)
                {
                    refreshCustomPipelines();
                    master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                }
                SitecoreResponse response = Search(master, obj);

                return Json(RenderFacetList(response, obj));
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }
        #endregion

        #region RenderFacetView
        private void RenderFacetView(SitecoreResponse response, string facetField, JsonResponse json, string partial)
        {
            FacetView facetView = new FacetView();
            List<string> facets = new List<string>();
            if (response.Facets.TryGetValue(facetField, out facets))
            {
                string filter = "";
                if (response.OriginalQuery.Filters.TryGetValue(facetField, out filter))
                {
                    facetView.Filter = filter.Split(',').ToList();
                }

                facetView.Facet = new KeyValuePair<string, List<string>>(facetField, facets);
                json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
            }
        }
        #endregion

        #region RenderFacetList
        private Dictionary<string, List<string>> RenderFacetList(SitecoreResponse response, SitecoreQuery obj)
        {
            Dictionary<string, List<string>> Facets = new Dictionary<string, List<string>>();
            foreach (string facetField in obj.Facets)
            {
                List<string> facets = new List<string>();
                if (response.Facets.TryGetValue(facetField, out facets))
                {
                    Facets.Add(facetField, facets);
                }
            }

            return Facets;
        }
        #endregion

        #region BuildJsonErroResponse
        private JsonResult BuildJsonErrorResponse(Exception e)
        {
            Dictionary<string, object> error = new Dictionary<string, object>();
            error.Add("Status", "Error");
            return Json(error);
        }
        #endregion

        #region Download
        public FileResult Download(string FilePath)
        {
            try
            {
                FilePath = "/sitecore/media library" + FilePath;
                Sitecore.Data.Database current = Sitecore.Context.Database;
                Sitecore.Data.Items.Item item = Sitecore.Context.Database.Items[FilePath];
                if (item != null)
                {
                    Sitecore.Data.Items.Item sampleMedia = new Sitecore.Data.Items.MediaItem(item);
                    var mediaItem = (MediaItem)sampleMedia;
                    Stream stream = mediaItem.GetMediaStream();
                    long fileSize = stream.Length;
                    byte[] buffer = new byte[(int)fileSize];
                    stream.Read(buffer, 0, (int)stream.Length);
                    stream.Close();
                    string fileName = mediaItem.Name + "." + mediaItem.Extension;
                    return File(buffer, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Controllers.ReportCenterController :: Download -> Exception : " + ex.ToString(), ex, ex.Data);
                return null;
            }

        }
        #endregion
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public MediaItem AddFile(string fileName, string sitecorePath, string mediaItemName)
        {
            try
            {
                // Create the options
                Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                // Store the file in the database, not as a file
                options.FileBased = false;
                // Remove file extension from item name
                options.IncludeExtensionInItemName = false;
                // Overwrite any existing file with the same name
                options.OverwriteExisting = true;
                // Do not make a versioned template
                options.Versioned = false;
                // set the language
                options.Language = Sitecore.Globalization.Language.Current;
                // set the path
                options.Destination = sitecorePath + "/" + mediaItemName;
                // Set the database
                options.Database = Sitecore.Configuration.Factory.GetDatabase("master");



                // Now create the file
                Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                Console.WriteLine("Creating Media");
                fileName = Path.GetFullPath(fileName);
                MediaItem mediaItem = creator.CreateFromFile(fileName, options);

                //Edit mediaItem to include Title
                using (new Sitecore.SecurityModel.SecurityDisabler()) {
                    try
                    {
                        mediaItem.BeginEdit();
                        mediaItem.InnerItem.Fields["Title"].Value = mediaItemName;
                        mediaItem.EndEdit();
                    }
                    catch (Exception ex)
                    {
                        Sitecore.Diagnostics.Log.Error("Could not update item " + mediaItem.MediaPath + ": " + ex.Message, mediaItem);
                        mediaItem.EndEdit();
                    }
                }

                PublishItem(mediaItem);

                Console.WriteLine("MEDIA CREATED");
                return mediaItem;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.InnerException);
                Console.WriteLine("INNER INNER EXCEPTION: " + ex.InnerException.InnerException);

                Console.WriteLine("INNER INNER INNER EXCEPTION: " + ex.InnerException.InnerException.InnerException);
                Console.ReadLine();
                return null;
            }
        }

        private static void PublishItem(MediaItem item)
        {
            // The publishOptions determine the source and target database,
            // the publish mode and language, and the publish date
            Sitecore.Publishing.PublishOptions publishOptions =
              new Sitecore.Publishing.PublishOptions(item.Database,
                                                     Sitecore.Data.Database.GetDatabase("master"),
                                                     Sitecore.Publishing.PublishMode.SingleItem,
                                                     Sitecore.Globalization.Language.Current,
                                                     System.DateTime.Now);  // Create a publisher with the publishoptions
            Sitecore.Publishing.Publisher publisher = new Sitecore.Publishing.Publisher(publishOptions);

            // Choose where to publish from
            publisher.Options.RootItem = item;

            // Publish children as well?
            publisher.Options.Deep = true;

            // Do the publish!
            publisher.Publish();
        }
    }
}