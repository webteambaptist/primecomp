﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class PhysicianSearch : Physician
    {
        public string ContentID { get; set; }
        public string Template { get; set; }
        public string Path { get; set; }
        public float Distance { get; set; }
    }
}