﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class BlogPost
    {
        public string title { get; set; }
        public string Summary { get; set; }
        public string Category { get; set; }
        public string Body { get; set; }
        public string PublishDate { get; set; }
        public string ItemID { get; set; }
    }
}