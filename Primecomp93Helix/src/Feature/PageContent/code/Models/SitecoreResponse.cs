﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class SitecoreResponse
    {
        public SitecoreResponse()
        {
            Facets = new Dictionary<string, List<string>>();
        }
        public List<Dictionary<string, object>> Results { get; set; }
        public Dictionary<string, List<string>> Facets { get; set; }
        public SitecoreQuery OriginalQuery { get; set; }
        public int TotalHits { get; set; }
        public string Pagination { get; set; }
    }
}