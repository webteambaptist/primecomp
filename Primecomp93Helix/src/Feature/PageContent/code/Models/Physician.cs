﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class Physician : User
    {
        private string _firstName;

        public string GetFirstName()
        {
            return _firstName;
        }

        public void SetFirstName(string value)
        {
            _firstName = value;
        }

        private string _middleName;

        public string GetMiddleName()
        {
            return _middleName;
        }

        public void SetMiddleName(string value)
        {
            _middleName = value;
        }

        private string _lastName;

        public string GetLastName()
        {
            return _lastName;
        }

        public void SetLastName(string value)
        {
            _lastName = value;
        }

        public new string Gender { get; set; }
        public string Specialty { get; set; }
        public string SubSpecialty { get; set; }
        public string SpecialtyNotes { get; set; }
        public string Fax { get; set; }
        public string City { get; set; }
        public string County { get; set; }

        private string _phone;

        public string GetPhone()
        {
            return _phone;
        }

        public void SetPhone(string value)
        {
            _phone = value;
        }

        public string Address { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AdditionalFacilityAddress { get; set; }
        public string AdditionalFacilityState { get; set; }
        public string AdditionalFacilityZip { get; set; }
        public string AdditionalFacilityCity { get; set; }
        public string AdditionalFacilityFax { get; set; }
        public string AdditionalFacilityPhone { get; set; }
        public string SecondaryFacilityAddress { get; set; }
        public string SecondaryFacilityState { get; set; }
        public string SecondaryFacilityZip { get; set; }
        public string SecondaryFacilityCity { get; set; }
        public string SecondaryFacilityFax { get; set; }
        public string SecondaryFacilityPhone { get; set; }
        public string GroupName { get; set; }
        public string ProviderType { get; set; }
        public string Degree { get; set; }
        public string[] Languages { get; set; }
        public string site { get; set; }
        #region Physician Specific Properties
        //public string ProviderType { get; set; }
        //public string Degree { get; set; }
        //public string Specialty { get; set; }
        //public string SubSpecialty { get; set; }
        //public string JobTitle { get; set; }

        //public string GroupName { get; set; }
        //public string SpecialtyNotes { get; set; }
        public string ID { get; set; }
        ////public string[] OtherLocations { get; set; }
        //public LocationInformation Location { get; set; }
        //public LocationInformation AdditionalFacility { get; set; }
        //public LocationInformation SecondaryFacility { get; set; }
        #endregion
    }
    //public class LocationInformation
    //{
    //    public string County { get; set; }
    //    public string PhysicalAddress { get; set; }
    //    public string City { get; set; }
    //    public string State { get; set; }
    //    public string Zip { get; set; }
    //    public string Phone { get; set; }
    //    public string Fax { get; set; }
    //}

}