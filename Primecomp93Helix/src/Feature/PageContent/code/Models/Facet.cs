﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class Facet
    {
        public Facet()
        {
            facet = new KeyValuePair<string, List<string>>();
            //Filter = "";
            Filter = new List<string>();
        }

        public KeyValuePair<string, List<string>> facet { get; set; }
        //public string Filter { get; set; }
        public List<string> Filter { get; set; }
    }
}