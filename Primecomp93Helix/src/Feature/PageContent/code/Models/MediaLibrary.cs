﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class MediaLibrary
    {
        public List<MediaFiles> FileList { get; set; }
    }
    public class MediaFiles
    {
        public string Title { get; set; }
        public string Path { get; set; }
    }
}