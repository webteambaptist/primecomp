﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class Header
    {
        public Menu Menu { get; set; }
        public List<MenuItem> MenuItems { get; set; }
        public List<Nav> Nav { get; set; }
    }

    public class Menu
    {
        public string SiteLogo { get; set; }
        public string SiteLogoURL { get; set; }
        public bool GetAuthMenu { get; set; }
        public List<MenuItem> MenuItem { get; set; }
    }
    public class MenuItem
    {
        public string MenuItemText { get; set; }
        public string MenuItemLogo { get; set; }
        public string MenuItemURL { get; set; }
        public MenuItem()
        {
            MenuItemText = "";
            MenuItemLogo = "";
            MenuItemURL = "";
        }
    }   
    public class Nav
    {
        public string NavItemText { get; set; }
        public string NavItemURL { get; set; }
        public List<NavLinks> NavLinks { get; set; }
    }
    public class NavLinks
    {
        public string NavLinkText { get; set; }
        public string NavLinkURL { get; set; }
    }
    public class Breadcrumb
    {
        public List<Crumb> Crumtrail { get; set; }
    }
    public class Crumb
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
    }
}