﻿using Sitecore.Data.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class CarouselComponent
    {
        public string ImageTitle { get; set; }
        public string ImageText { get; set; }
        public ImageField ImgField { get; set; }
    }
    public class CarouselComponentItems
    {
        public List<CarouselComponent> _Items { get; set; }
    }
}