﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class Document
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string BackupTitle { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public string ThumbnailIconPath { get; set; }
        public string Extension { get; set; }
        public ArrayList Tags { get; set; }
        public string Template { get; set; }
        public string Path { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}