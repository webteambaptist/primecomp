﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.DirectoryServices.AccountManagement;
namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class Login
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public static bool IsValid(string username, string password)
        {
            bool isValid = false;
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "BH"))
            {
                isValid = pc.ValidateCredentials(username, password);
            }
        return isValid;
        }
    }
}