﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class Posts
    {
        public List<Item> _industry { get; set; }
        public List<Item> _community { get; set; }
    }
}