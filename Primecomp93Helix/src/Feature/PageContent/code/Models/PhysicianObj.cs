﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class PhysicianObj
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Specialty { get; set; }
        public string SubSpecialty { get; set; }
        public string SpecialtyNotes { get; set; }
        public string Fax { get; set;}
        public string City { get; set; }
        public string County { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AdditionalFacilityAddress { get; set; }
        public string AdditionalFacilityState { get; set; }
        public string AdditionalFacilityZip { get; set; }
        public string AdditionalFacilityCity { get; set; }
        public string AdditionalFacilityFax { get; set; }
        public string AdditionalFacilityPhone { get; set; }
        public string SecondaryFacilityAddress { get; set; }
        public string SecondaryFacilityState { get; set; }
        public string SecondaryFacilityZip { get; set; }
        public string SecondaryFacilityCity { get; set; }
        public string SecondaryFacilityFax { get; set; }
        public string SecondaryFacilityPhone { get; set; }
        public string GroupName { get; set; }
        public string ProviderType { get; set; }
        public string Degree { get; set; }
        public string Languages { get; set; }
        public string Distance { get; set; }
        public string Path { get; set; }
        public string ContentID { get; set; }
    }
}