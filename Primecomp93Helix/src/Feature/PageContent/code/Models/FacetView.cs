﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class FacetView
    {
        public FacetView()
        {
            Facet = new KeyValuePair<string, List<string>>();
            //Filter = "";
            Filter = new List<string>();
        }

        public KeyValuePair<string, List<string>> Facet { get; set; }
        //public string Filter { get; set; }
        public List<string> Filter { get; set; }
    }
}