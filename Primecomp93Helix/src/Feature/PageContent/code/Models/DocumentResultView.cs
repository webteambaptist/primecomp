﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class DocumentResultView
    {
        public DocumentResultView()
        {
            Start = 0;
            Documents = new List<Document>();
        }

        public int Start { get; set; }
        public List<Document> Documents { get; set; }
    }
}
