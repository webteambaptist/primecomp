﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Primecomp93Helix.Feature.PageContent.Models
{
    public class ZipCode
    {
        public double lat { get; set; }
        public double lon { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCity { get; set; }

    }
}