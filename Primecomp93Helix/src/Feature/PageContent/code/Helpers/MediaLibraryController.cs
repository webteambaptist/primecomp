﻿using Primecomp93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Primecomp93Helix.Feature.PageContent.Helpers
{
    public class MediaLibraryController : Controller
    {
        // GET: MediaLibrary
        public static List<MediaFiles> GetMediaArticles(ID ArticleFolderID)
        {
            List<MediaFiles> articleList = new List<MediaFiles>();

            Database db = Sitecore.Context.Database;

            Item articleFolder = Sitecore.Context.Database.GetItem(ArticleFolderID);
            var children = articleFolder.Children;
            var innerChildren = children.InnerChildren;

            for (int i = 0; i < innerChildren.Count; i++)
            {
                string url = MediaManager.GetMediaUrl(innerChildren[i]);
                MediaFiles a = new MediaFiles();
                a.Title = children[i].Name;
                a.Path = url;
                articleList.Add(a);
            }


            return articleList;
        }
    }
}