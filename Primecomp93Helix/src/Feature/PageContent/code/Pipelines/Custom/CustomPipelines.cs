﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Pipelines;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Sitecore.Configuration;
using Sitecore.Diagnostics;

namespace Primecomp93Helix.Feature.PageContent.Pipelines.Custom
{
    public class CustomPipelines
    {
        private static readonly string StrProviderParentId = Settings.GetSetting("providerParentID");
        public void Process(PipelineArgs args)
        {
            LoadFAP();
        }
        private void LoadFAP()
        {
            Log.Info("Starting Custom Pipelines", this);
            try
            {
                var fap = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (fap == null)
                {
                    Database db = Sitecore.Configuration.Factory.GetDatabase("web");
                    Item parentItem = db.GetItem(StrProviderParentId);

                    var children = parentItem.GetChildren()
                        .OrderBy(f => f.Fields["LastName"].ToString())
                        .ThenBy(f => f.Fields["FirstName"].ToString())
                        .ToList();
                    //var expiresAt = DateTime.UtcNow.Date.AddHours(1);
                    var now = DateTime.Now;
                    DateTime expiresAt = now.AddDays(1);
                    System.Web.HttpContext.Current.Cache.Insert(
                        "sitecoredata", children, null, expiresAt, Cache.NoSlidingExpiration);
                }

            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("PrimeComp93Helix.Feature.PageContent.Pipelines.Custom.CustomPipelines.LoadFAP: ", e.InnerException.Message);
            }
        }
    }
}