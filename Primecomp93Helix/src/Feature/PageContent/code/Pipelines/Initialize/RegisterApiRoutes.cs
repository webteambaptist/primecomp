﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Pipelines;

namespace Primecomp93Helix.Feature.PageContent.Pipelines.Initialize
{
    public class RegisterApiRoutes : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        private static readonly string StrProviderParentId = Settings.GetSetting("providerParentID");
        public override void Process(PipelineArgs args)
        {
            Sitecore.Diagnostics.Log.Info("Starting Register API Routes Pipeline", this);
            try
            {
                Database db = Sitecore.Configuration.Factory.GetDatabase("web");
                Item parentItem = db.GetItem(StrProviderParentId);

                var children = parentItem.GetChildren()
                    .OrderBy(f => f.Fields["LastName"].ToString())
                    .ThenBy(f => f.Fields["FirstName"].ToString())
                    .ToList();
                System.Web.HttpContext.Current.Cache.Insert("sitecoredata", children);
                
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("PrimeComp93Helix.Feature.PageContent.Pipelines.Initialize.RegisterApiRoutes.Process: ", e.InnerException.Message);
            }
        }
    }
}